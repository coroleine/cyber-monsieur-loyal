# Cyber Monsieur Loyal
L’objectif de ce projet consiste a d'evelopper un outil de gestion à destination des organsateurs de tournois de programmation à l'IUT informatique. Il a pour objctif d'optimiser le temps passé à réaliser la composition des équipes qui doivent s'affronter.


### A. Membres du Projet
#### 1. Les Etudiants
- BERNARD Quentin
- HENRIQUES Alan
- ORLAY Arnaud
- PIROIRD Romain
- TISSIER Dorian
- VERNADAT Mickaël

#### 2. Les Enseignants
- BROSSIER Nathalie
- BECKER Florent


### B. Technologies
- `Flask` ([documentation](http://flask.pocoo.org/docs/1.0/quickstart/))
- `Materialize` ([documentation](https://materializecss.com/))
- `jQuery` ([documentation](https://api.jquery.com/))
- `API Graph` ([documentation](https://developers.facebook.com/docs/graph-api?locale=fr_FR))


### C. Installation
```bash
git clone https://gitlab.com/IUT-2019/cyber-monsieur-loyal.git
cd cyber-monsieur-loyal
./setup.sh # Executer le fichier d'installation
```

### D. Execution
```bash
./run.sh # Exucuter le fichier de lancement de l'application
# L'application est accesible sur https://localhost:5000
```


### F. Proxy
Si vous êtes derrière un proxy, n'oubliez pas de renseigner la variable d'environnement `HTTP_PROXY` (en majuscules).


### G. Lien avec Facebook®
Pour pouvoir faire une publication sur une page Facebook® via notre application, il faut se connecter avec un compte Facebook® qui est administrateur de la page puis créer une application Facebook Developers et lié l'application Facebook Developers avec la page Facebook®. Il faut ensuite récupérer un token d'accès à la page sur l'explorateur de l'API Graph(https://developers.facebook.com/tools/explorer/) puis l'étendre à deux mois. Ensuite il faut échanger le nouveau token avec l'ancien(qui est expiré) dans le fichier "publierFacebook.js".
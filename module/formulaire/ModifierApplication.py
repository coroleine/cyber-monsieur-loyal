#### HEAD ##############
# Class   : ModifierApplication
# Content : Formulaire de modification de l'apparence de l'application


#### IMPORT ############
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms.validators import DataRequired, Length
from wtforms import StringField, validators
from flask_uploads import IMAGES



#### CLASS #############
class ModifierApplication(FlaskForm):
    nom = StringField(
        "Nom",
        validators = [
            DataRequired("S'il vous plaît, entrer le nom de l'application."),
            Length(min=3, max=50, message="Le nom doit comporter entre 3 et 50 caractères.")
        ]
    )
    logo = FileField(
        "logo",
        validators = [
            FileAllowed(IMAGES, 'Seulement des images !')
        ]
    )
    fond = FileField(
        "fond",
        validators = [
            FileAllowed(IMAGES, 'Seulement des images !')
        ]
    )


    def __repr__(self):
        return "<ModifierApplication (%s)>" % (self.nom.data)

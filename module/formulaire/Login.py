#### HEAD ##############
# Class   : LoginForm
# Content : Formulaire de création d'une connexion d'un organisateur

#### IMPORT ############
from wtforms import PasswordField, StringField, HiddenField
from hashlib import sha256
from flask_wtf import FlaskForm
from ..service_models import Organisateur

#### CLASS #############
class Login(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    next = HiddenField()

    def get_authenticated_user(self):
        """
            Compare le mot de passe de l'organisateur qui essai de se connecter à celui dans la base de donnée
            pour lui permettre de se connecter.

            :param self: le formulaire Login
            :type self: Object
        """
        user = Organisateur.query.filter_by(loginO=self.username.data).first()
        if user!=None:
            passwd = sha256(self.password.data.encode()).hexdigest()
            if  passwd == user.mdpO:
                return user
        return None

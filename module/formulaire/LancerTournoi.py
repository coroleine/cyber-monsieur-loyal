#### HEAD ##############
# Class   : LancerTournoi
# Content : Formulaire de lancement d'un tournoi.


#### IMPORT ############
from flask_wtf import FlaskForm
from wtforms.validators import DataRequired, Length
from wtforms import StringField, SelectField, validators


#### CLASS #############
class LancerTournoi(FlaskForm):
    equipes = SelectField(
        "Equipes",
        choices = List()
    )
    cartes = SelectField(
        "Cartes",
        choices = list()
    )

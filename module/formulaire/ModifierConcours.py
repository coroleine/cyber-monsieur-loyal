#### HEAD ##############
# Class   : ModifierConcours
# Content : Formulaire de modification d'un concours.


#### IMPORT ############
from flask_wtf import FlaskForm
from wtforms.validators import DataRequired, Length
from flask_wtf.file import FileField, FileAllowed
from wtforms import StringField, IntegerField, FloatField, TimeField, validators

#### CLASS #############
class ModifierConcours(FlaskForm):
    idC = IntegerField("Identifiant")

    nom = StringField(
        "Nom",
        validators = [
            DataRequired("S'il vous plaît, entrer le nom du concours."),
            Length(min=3, max=40, message="Le nom du concours doit comporter 3 caratères minimum et 40 maximum.")
        ]
    )

    avatar = FileField(
        "Avatar",
        validators = [
            FileAllowed(['jpg', 'png'], 'Seulement des images !')
        ]
    )

    dateDebut = StringField(
        "dateDebut",
        validators = [
            DataRequired("S'il vous plaît, entrer la date du debut du concours.")
        ]
    )

    dateFin = StringField(
        "dateFin",
        validators = [
            DataRequired("S'il vous plaît, entrer la date de fin du concours.")
        ]
    )

    heureDebut = TimeField(
        "heureDebut",
        validators = [
            DataRequired("S'il vous plaît, entrer l'heure de debut du concours.")
        ]
    )

    heureFin = TimeField(
        "heureFin",
        validators = [
            DataRequired("S'il vous plaît, entrer l'heure de fin du concours.")
        ]
    )


    tempsMatch = StringField(
        "tempsMatch",
        validators = [
            Length(min=2, max=3, message="Le temps de match doit durer entre 10 et 999 secondes")
        ]
    )

    nbrEquipeParMatch = StringField(
        "nbrEquipeParMatch",
        validators = [
            Length(min=1, max=3, message="Le temps de match doit avoir entre 1 et 999 equipes")
        ]
    )

    emplacementCarte = StringField(
        "emplacementCarte",
        validators = [
            Length(min=1, max=255, message="Le chemin des cartes du jeu doit contenir entre 1 et 255 caractères")
        ]
    )

    commandeLancement = StringField(
        "commandeLancement",
        validators = [
            Length(min=1, max=255, message="Le chemin du fichier de lancement doit contenir entre 1 et 255 caractères")
        ]
    )

    sujetConcours = FileField(
        "sujetConcours",
        validators = [
            FileAllowed(['pdf'], 'Seulement des pdf !')
        ]
    )

    participation = FloatField(
        "participation",
        validators = [
            DataRequired("S'il vous plaît, entrer la participation des équipes au tournoi."),
            Length(min=1, message="Le prix doit contenir au moins 1 caractère (si aucun prix mettre 0).")
        ]
    )

    portServeur = StringField(
        "port",
        validators = [
            Length(min=1, max=5, message="Le port du serveur doit etre compris entre 1 et 9999")
        ]
    )

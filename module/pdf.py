#### HEAD ##############
# File : pdf
# Content : Une méthode d'enregistrement des résultats


#### IMPORT ############
from .app import db
from xhtml2pdf import pisa
from io import StringIO
from datetime import date


#### METHODES ##########
def create_pdf(source, concours):
    # Définir le nom du fichier
    objet_date = date.today()
    file_date = str(objet_date.year) + "-" + str(objet_date.month) + "-" + str(objet_date.day)
    filename = file_date + "_" + concours.nomC + ".pdf"
    
    # Conversion du HTML en PDF
    resultFile = open("module/archive/" + filename, "w+b")
    pisaStatus = pisa.CreatePDF(source, dest=resultFile)
    resultFile.close()

    # Enregistrement de l'url dans la BD
    concours.resultats = filename
    db.session.commit()

/**
 * Name: modalEquipe
 * Content: - Une méthode de chargement du modal en fonction du bouton associé à une équipe.
 *          - Une méthode de confirmation de la suppression d'une équipe.
 */



 /**
  * Chargement du modal
  */
let instances;
document.addEventListener('DOMContentLoaded', function() {
    // Le modal
    let elems = document.querySelectorAll('.modal');
    instances = M.Modal.init(elems);
    
    // Les boutons
    let collection = document.querySelector(".collection"); // La liste des joueurs
    let liste_form = document.querySelectorAll(".btn-form"); // La liste des boutons (équipes)

    liste_form.forEach(form => {
        form.addEventListener("click", function(){
            // On supprime de caractère disable du bouton
            document.querySelector("#supprimer-equipe").parentNode.classList.remove("disabled");
             
            // Requete AJAX vers le serveur pour récupérer les informations
            // sur l'équipe appartenant au concours.
            $.ajax({
                url: "/concours/" + form.getAttribute("id-concours") + "/equipes/load-group/",
                data: {"idEquipe": form.getAttribute("id-equipe")},
                type: 'POST',

                // Si il n'y a pas d'erreur
                success: function(response){
                    objet = JSON.parse(response) // Transforme le texte en objet

                    // Insertion des données pour l'image représentant l'avatar
                    // de l'équipe ainsi que les champs de texte pour le nom de
                    // l'équipe et son poste machine qui lui est attribué.
                    document.querySelector("#supprimer-equipe").setAttribute("id-concours", form.getAttribute("id-concours"))
                    document.querySelector("#id-equipe").value = form.getAttribute("id-equipe");
                    document.querySelector("#modif-avatar-label").style.backgroundImage = "url('/static/img/upload/equipe/" + objet["avatar"] + "')";
                    document.querySelector("#label-modif-team-name").classList.add("active")
                    document.querySelector("#modif-team-name").setAttribute("value", objet["nom"]);
                    if(objet["poste"] != ""){
                        document.querySelector("#label-modif-team-post").classList.add("active")
                    }
                    else{
                        document.querySelector("#label-modif-team-post").classList.remove("active")
                    }
                    document.querySelector("#modif-team-post").setAttribute("value", objet["poste"]);

                    // suppression des éléments de liste (joueurs) avant d'en charger de nouveaux.
                    let taille = collection.childElementCount;
                    for(i=taille-1; i>=0; i--){
                        collection.removeChild(collection.children[i]);
                    }

                    // Pour chaque joueur dans l'équipe, on crée une item (élément de liste)
                    // que l'on ajoute à la balise 'ul' nommé "collection".
                    objet["joueurs"].forEach(joueur => {
                        let item = document.createElement("li");
                        item.setAttribute("class", "collection-item");
                        let conteneur = document.createElement("div");
                        let texte_conteneur = document.createTextNode(joueur["nomJ"] + " " + joueur["prenomJ"]);
                        let formulaire = document.createElement("form");
                        formulaire.setAttribute("class", "secondary-content");
                        formulaire.addEventListener("click", function(){
                            // Requete AJAX vers le serveur pour supprimer dans la base de données
                            // le joueur sur lequel l'icon corbeil a été cliqué.
                            $.ajax({
                                url: "/concours/" + form.getAttribute("id-concours") + "/equipes/delete-player/",
                                data: {"idJoueur": joueur["idJ"]},
                                type: 'POST',

                                // Si il n'y a pas d'erreur
                                success: function(response){
                                    let message;
                                    if(JSON.parse(response)["delete"]){
                                        collection.removeChild(item);
                                        message = "<i class='material-icons'>done</i><span class='white-text'>" + joueur["nomJ"] + " " + joueur["prenomJ"] + " supprimé</span>"
                                        M.toast({html: message, classes: 'message-success', displayLength: 1000});
                                    }
                                    else{
                                        message = "<i class='material-icons'>error</i><span class='white-text'>Erreur de suppression</span>"
                                        M.toast({html: message, classes: 'message-error', displayLength: 2000});
                                        console.log(response);
                                    }
                                },

                                // Si il y as une erreur
                                error: function(error){
                                    console.log(error);
                                    message = "<i class='material-icons'>error</i><span class='white-text'>Erreur de suppression</span>"
                                    M.toast({html: message, classes: 'message-error', displayLength: 2000})
                                }
                            });
                        });

                        let icon = document.createElement("i");
                        icon.setAttribute("class", "material-icons");
                        let texte_icon = document.createTextNode("delete");
                        icon.appendChild(texte_icon);
                        formulaire.appendChild(icon);
                        conteneur.appendChild(texte_conteneur);
                        conteneur.appendChild(formulaire);
                        item.appendChild(conteneur);
                        collection.appendChild(item);
                    });
                },

                // Si il y as une erreur
                error: function(error){
                    console.log(error);
                }
            });
        });
    });
});







/**
 * Affichage d'une notification de suppression de l'équipe.
 * Possibilité d'annuler la suppression.
 */
let supprimer = document.querySelector("#supprimer-equipe");
supprimer.addEventListener("click", function(){
    supprimer.parentElement.classList.add("disabled")
    let nomEquipe = document.querySelector("#modif-team-name").value;
    let idEquipe = parseInt(document.querySelector("#id-equipe").value);

    let liste_bouton = document.querySelectorAll(".btn-form")
    liste_bouton.forEach(elem => {
        if(elem.getAttribute("id-equipe") == idEquipe){
            elem.style.display = "none";
        }
    });

    instances[0].close();
    
    // Affichage d'un toast. Il réalise la function suivante 
    // lorsque le temps d'affichage du message a expiré.
    let idBouton = "annuler-suppression-" + idEquipe;
    let message = "<i class='material-icons'>warning</i><span class='white-text'>" + nomEquipe + " supprimé</span><button class='btn-flat toast-action modal-trigger' id='" + idBouton +"'>Annuler</button>";
    let annuler = false;

    M.toast({html: message, classes: "message-warning", displayLength: 3000, completeCallback:function(){
        // Requette pour supprimer l'équipe si le toast n'est plus affiché et
        // que le bouton n'a pas été préssé.
        if(!annuler){
            $.ajax({
                url: "/concours/" + supprimer.getAttribute("id-concours") + "/equipes/delete-group/",
                data: {"idEquipe": idEquipe},
                type: 'POST',
                success: function(response){
                    let message;
                    if(!JSON.parse(response)["delete"]){
                        message = "<i class='material-icons'>error</i><span class='white-text'>Erreur de suppression</span>"
                        M.toast({html: message, classes: 'message-error', displayLength: 2000});
                        console.log(response);
                    }
                },
                error: function(error){
                    console.log(error);
                    message = "<i class='material-icons'>error</i><span class='white-text'>Erreur de suppression</span>"
                    M.toast({html: message, classes: 'message-error', displayLength: 2000})
                }
            });
        }
    }});

    // Actions réalisé si l'utilisateur clique sur le bouton "annuler".
    let bouton_annuler = document.querySelector("#" + idBouton);
    bouton_annuler.addEventListener("click", function(){
        annuler = true;

        // Retirer le "disable" sur le bouton "supprimer" pour le ré-afficher
        supprimer.parentElement.classList.remove("disabled")
        
        // Chacher le toast.
        var toastElement = bouton_annuler.parentElement;
        var toastInstance = M.Toast.getInstance(toastElement);
        toastInstance.dismiss();

        // Cacher le bouton d'affichage du modal
        let liste_bouton = document.querySelectorAll(".btn-form")
        liste_bouton.forEach(elem => {
            if(elem.getAttribute("id-equipe") == idEquipe){
                elem.style.display = "block";
            }
        });

        // Cacher le modal
        instances[0].open();

    });

});
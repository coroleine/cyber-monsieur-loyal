document.addEventListener('DOMContentLoaded', function() {

    // On récupère la liste des joueurs solo (collection de bouton)
    let liste_joueur_solo = new Array();
    let ensemble_joueur = document.querySelector("section.affichage-joueur");
    if(ensemble_joueur != null){
        liste_joueur_solo = Array.from(ensemble_joueur.children);

    }

    // On supprime le contexteMenu lorsque le bouton pert le "focus".
    document.addEventListener("click", function(event){
        if(!liste_joueur_solo.includes(event.target)){
            if(event.target != document.querySelector(".contexte-menu div.collapsible-header")){
                closeContexteMenu();
            }
        }
    });
    
    // Pour chacun d'entre eux :
    liste_joueur_solo.forEach(joueur_solo => {    
        
        // On vas créer un contexteMenu
        joueur_solo.addEventListener("click", function(){

            closeContexteMenu();

            // Création du conteneur
            let contexte_menu = document.createElement("ul");
            contexte_menu.className = "collapsible collapsible-accordion contexte-menu z-depth-2";
            M.Collapsible.init(contexte_menu);

            // Création de l'item de déplacement et de sont contenu
            let liste_equipe = getEquipes();
            let item_deplacer = document.createElement("li");
            let item_deplacer_header = document.createElement("div");
            let item_deplacer_header_icon = document.createElement("i");
            let item_deplacer_header_icon_type = document.createTextNode("folder_shared");
            let item_deplacer_header_text = document.createTextNode("Déplacer vers");
            let item_deplacer_body = document.createElement("div");
            let item_deplacer_body_contener = document.createElement("span");
            let liste_item = document.createElement("div");
            for(i in liste_equipe){
                let item = document.createElement("a");
                item.href = "/concours/" + liste_equipe[i].idConcours + "/equipes/"  + liste_equipe[i].idEquipe + "/joueur/" + joueur_solo.getAttribute("id-joueur")
                item.classList.add("collection-item");
                let item_text = document.createTextNode(liste_equipe[i].nom);
                item.appendChild(item_text);
                liste_item.appendChild(item);
            }
            liste_item.className = "collection";
            item_deplacer_header.className = "collapsible-header";
            item_deplacer_header_icon.className = "material-icons";
            item_deplacer_body.className = "collapsible-body";
            item_deplacer_header_icon.appendChild(item_deplacer_header_icon_type);
            item_deplacer_header.appendChild(item_deplacer_header_icon);
            item_deplacer_header.appendChild(item_deplacer_header_text);
            item_deplacer_body.appendChild(item_deplacer_body_contener);
            item_deplacer_body_contener.appendChild(liste_item);
            item_deplacer.appendChild(item_deplacer_header);
            item_deplacer.appendChild(item_deplacer_body);
            
            // Création de l'item de suppession
            let item_supprimer = document.createElement("li");
            let item_supprimer_header = document.createElement("div");
            let item_supprimer_header_icon = document.createElement("i");
            let item_supprimer_header_icon_type = document.createTextNode("delete");
            let item_supprimer_header_text = document.createTextNode("Supprimer");
            item_supprimer_header.className = "collapsible-header";
            item_supprimer_header_icon.className = "material-icons";
            item_supprimer_header_icon.appendChild(item_supprimer_header_icon_type);
            item_supprimer_header.appendChild(item_supprimer_header_icon);
            item_supprimer_header.appendChild(item_supprimer_header_text);
            item_supprimer.appendChild(item_supprimer_header);

            
            // Ajout des deux items dans la liste
            contexte_menu.appendChild(item_deplacer);
            contexte_menu.appendChild(item_supprimer);
            document.body.appendChild(contexte_menu);
            
            // Placer le contexte-menu en x et y dans la page
            contexte_menu.style.left = joueur_solo.getBoundingClientRect().left + scrollX - ((Math.abs(joueur_solo.getBoundingClientRect().width - contexte_menu.getBoundingClientRect().width))/2) + "px";
            contexte_menu.style.bottom = -scrollY + (document.body.offsetHeight-joueur_solo.offsetTop) + 20 + "px";

            // ==================== Méthode de suppression d'un joueur
            item_supprimer.addEventListener("click", function(){
                let idConcours = joueur_solo.getAttribute("id-concours");
                let idJoueur = joueur_solo.getAttribute("id-joueur");
                let nomJoueur = joueur_solo.childNodes[0].data;
                $.ajax({
                    url: "/concours/" + idConcours + "/equipes/delete-player/",
                    data: {"idJoueur": idJoueur},
                    type: 'POST',
                    success: function(response){
                        let message;
                        if(JSON.parse(response)["delete"]){
                            closeContexteMenu();
                            joueur_solo.remove();
                            if(document.querySelector("section.affichage-joueur").childElementCount == 0){
                                document.querySelector("body > main > article:nth-child(4)").remove();
                            }
                            message = "<i class='material-icons'>done</i><span class='white-text'>" + nomJoueur + " supprimé</span>"
                            M.toast({html: message, classes: 'message-success', displayLength: 1000});
                        }
                        else{
                            message = "<i class='material-icons'>error</i><span class='white-text'>Erreur de suppression</span>"
                            M.toast({html: message, classes: 'message-error', displayLength: 2000});
                            console.log(response);
                        }
                    },
                    error: function(error){
                        console.log(error);
                        message = "<i class='material-icons'>error</i><span class='white-text'>Erreur de suppression</span>"
                        M.toast({html: message, classes: 'message-error', displayLength: 2000});
                    }
                });
            });
            // ====================



            // ==================== Méthode de déplacement d'un joueur
            item_deplacer.addEventListener("click", function(){
                let taille = document.querySelector(".collapsible-body").getBoundingClientRect();
                if(!item_deplacer.classList.contains("active")){
                    contexte_menu.style.top = "calc(" + contexte_menu.style.top + " - " + taille +"px)";
                }
                else {
                    contexte_menu.style.top = "calc(" + contexte_menu.style.top + " + " + taille +"px)";
                }
            });
            // ====================
            
        });
    });
});


/**
 * Name: close
 * Ferme le contexte menu.
 */
function closeContexteMenu(){
    let precedent = document.querySelector(".contexte-menu");
    if(precedent != null){
        precedent.remove();
    }
}



/**
 * Name: getEquipes
 * Retourne une liste des équipe (type objet) dans lequel
 * on pourras déplacer le joueur.
 */
function getEquipes(){
    let equipes = new Array();
    let parent = document.querySelector("section.affichage-equipe");
    for(enfant of parent.children){
        let equipe = new Object();
        equipe.idConcours = enfant.getAttribute("id-concours");
        equipe.idEquipe = enfant.getAttribute("id-equipe");
        equipe.nom = enfant.firstElementChild.lastElementChild.childNodes[0].data;
        equipes.push(equipe);
    }
    return equipes;
}
/**
 * Name: Menu
 * Content: Met à jour l'état d'activation de l'onglet (situé dans le menu déroulant)
 *          en fonction de la page sur lequel l'utilisateur se trouve.
 */

document.addEventListener("DOMContentLoaded", function(){
    let page_name = window.location.pathname.split("/")[1];
    let liste_items = document.querySelectorAll("li.menu-item");

    for(item of liste_items){
        if("active" in item.classList){
            item.classList.remove("active");
        }
        if(item.getAttribute("menu-id") == page_name){
            item.classList.add("active");
        }
    }
});
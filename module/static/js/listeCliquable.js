document.addEventListener('DOMContentLoaded', function() {
    // ======================================== Balises
    // Balise <ul> comportant la liste des équipes
    let NeParticipePas = document.getElementById("neParticipePas");
    let Participe = document.getElementById("participe");

    // Tableau de la liste des équipes
    let listeNeParticipePas = neParticipePas.children;
    let listeParticipe = Participe.children;

    // Bouton de sélection et désélection des équipes
    let toutSelectionner = document.getElementById("toutSelectionner");
    let toutDeselectionner = document.getElementById("toutDeselectionner");

    // Compteur indiquant le nombre de joueur dans chaque liste
    let cptNonJoueur = document.getElementById("cpt-non-joueur");
    let cptJoueur = document.getElementById("cpt-joueur");

    // Input select contenant les équipes
    let selectEquipe = document.getElementById("equipe");




    // ======================================== Nettoyage de la balise "select"
    // Vide le select pour ne pas garder les valeurs précédentes
    Array.from(selectEquipe.children).forEach(elem => {
        elem.selected = false;
    });

    // Récupères toutes les valeurs de Participe pour les ajouter à l'input select
    Array.from(listeParticipe).forEach(elem => {
      selectAddEquipe(elem);
    });




    // ======================================== Fonctions du select d'équipe + compteur
    /**
     * Recharge le compteur des deux listes.
     */
    function compteur(){
        cptNonJoueur.innerText = listeNeParticipePas.length;
        cptJoueur.innerText = listeParticipe.length;
    }

    /**
     * Sélectionne dans la balise "input" le nom de l'équipe
     * qui lui a été passé en paramètre.
     * @param {object} equipe 
     */
    function selectAddEquipe(equipe){
        Array.from(selectEquipe.children).forEach(elem => {
            if(elem.innerText == equipe.lastElementChild.innerText){
                elem.selected = true;
            }
        });
    }

    /**
     * désélectionne dans la balise "input" le nom de l'équipe
     * qui lui a été passé en paramètre.
     * @param {object} equipe 
     */
    function selectRemoveEquipe(equipe){
        Array.from(selectEquipe.children).forEach(elem => {
            if(elem.innerText == equipe.lastElementChild.innerText){
                elem.selected = false;
            }
        });
    }




    // ======================================== Boutons de sélection et désélection
    // Passe toutes les équipes non participantes du côté des équipes participantes
    toutSelectionner.addEventListener("click", function(event){
        Array.from(listeNeParticipePas).forEach(elem => {
            Participe.appendChild(elem);
            elem.removeEventListener("click", selectedForPlay, elem);
            elem.addEventListener("click", unselectedForPlay, elem);
            selectAddEquipe(elem);
        });
        toutDeselectionner.classList.remove("disabled");
        toutSelectionner.classList.add("disabled");
        compteur();
      });
  
    // Passe toutes les équipes participantes du côté des équipes non participantes
    toutDeselectionner.addEventListener("click", function(event){
        Array.from(listeParticipe).forEach(elem => {
            NeParticipePas.appendChild(elem);
            elem.removeEventListener("click", unselectedForPlay, elem);
            elem.addEventListener("click", selectedForPlay, elem);
            selectRemoveEquipe(elem);
        });
        toutSelectionner.classList.remove("disabled");
        toutDeselectionner.classList.add("disabled");
        compteur();
    });




    // ======================================== Fonction de transfer d'une équipe
    /**
     * Transfer une équipe non participante dans le groupe des participants
     * @param {object} equipe 
     */
    function selectedForPlay(event){
        let equipe = event.currentTarget;
        Participe.appendChild(equipe);
        selectAddEquipe(equipe);
        equipe.removeEventListener("click", selectedForPlay, equipe);
        equipe.addEventListener("click", unselectedForPlay, equipe);
        if (neParticipePas.children.length == 0) {
            toutSelectionner.classList.add("disabled");
        }
        toutDeselectionner.classList.remove("disabled");
        compteur();
    }

    /**
     * Transfer une équipe participan au match dans le groupe des nonparticipants
     * @param {object} equipe 
     */
    function unselectedForPlay(event){
        let equipe = event.currentTarget;
        NeParticipePas.appendChild(equipe);
        selectRemoveEquipe(equipe);
        equipe.removeEventListener("click", unselectedForPlay, equipe);
        equipe.addEventListener("click", selectedForPlay, equipe);
        if (Participe.children.length == 0) {
            toutDeselectionner.classList.add("disabled");
        }
        toutSelectionner.classList.remove("disabled");
        compteur();
    }




    // ======================================== Initialisation des eventListeners
    Array.from(listeNeParticipePas).forEach( elem => {
        elem.addEventListener("click", selectedForPlay, elem, true);
    });

    // Récupères toutes les valeurs de Participe pour les ajouter à l'input select
    Array.from(listeParticipe).forEach( elem => {
        elem.addEventListener("click", unselectedForPlay, elem, true);
    });


});
// change le thème pour l'application
function initColor(color) {
  let css_rules = document.styleSheets[2].cssRules[0].style;
  let old_css_rules_text = css_rules.cssText;
  let new_css_rules_text;
  let css_rules_array = old_css_rules_text.split(' --');
  let new_theme = "--theme: " + color + ";";

  css_rules_array[0] = new_theme;
  new_css_rules_text = css_rules_array.join(' --');
  css_rules.cssText = new_css_rules_text;
}

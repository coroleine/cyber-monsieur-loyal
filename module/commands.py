from .app import app, db
import click
from click.testing import CliRunner
from .service_models import *
from datetime import datetime

@app.cli.command()
@click.argument("name")
def execute_sql(name):
    """
    Execute un fichier passé en paramètre.
    """
    with open(name, "r") as file:
        file_content = file.read()
        for commande in file_content.split(";"):
            try:
                result = db.session.execute(commande)
            except:
                print("dans le fichier {}:\033[0;00m\n{}.".format(name, commande))
                exit(1)
    db.session.commit()
    exit(0)

@app.cli.command()
def crea_db():
    """ Crée toutes les tables et ajoute les éléments indispensables"""
    db.create_all()

    # Application
    db.session.add(Application(nomA="L'arène", backgroundConnexionA="terre.jpg", logoA="gladiator.svg", active=1))

    # Theme
    db.session.add(Theme(couleurT="#f44336"))
    db.session.add(Theme(couleurT="#e91e63"))
    db.session.add(Theme(couleurT="#9c27b0"))
    db.session.add(Theme(couleurT="#673ab7"))
    db.session.add(Theme(couleurT="#3f51b5"))
    db.session.add(Theme(couleurT="#2196f3"))
    db.session.add(Theme(couleurT="#03a9f4"))
    db.session.add(Theme(couleurT="#00bcd4"))
    db.session.add(Theme(couleurT="#009688"))
    db.session.add(Theme(couleurT="#4caf50"))
    db.session.add(Theme(couleurT="#8bc34a"))
    db.session.add(Theme(couleurT="#cddc39"))
    db.session.add(Theme(couleurT="#ffeb3b"))
    db.session.add(Theme(couleurT="#ffc107"))
    db.session.add(Theme(couleurT="#ff9800"))
    db.session.add(Theme(couleurT="#ff5722"))
    db.session.add(Theme(couleurT="#795548"))
    db.session.add(Theme(couleurT="#9e9e9e"))
    db.session.add(Theme(couleurT="#607d8b"))

    # Organisateur
    new_Organisateur("admin", "123456789", 15, 1)

    # Execution
    db.session.commit()

@app.cli.command()
def load_tests_db():
    """ Ajoute un jeu d'essai à la BD """

    # Concours
    db.session.add(Concours(nomC="Battle City",          dateDeb=datetime(2019,4,20,8,30),   dateFin=datetime(2019,4,20,18,30),  prixInscription=1.0,    image="battle-city.png",      nbrEquipeParMatch=4, tempsMatch=600, commandeLancement="url_serveur", repertoireCarte="url_cartes"))
    db.session.add(Concours(nomC="Tank Attack",          dateDeb=datetime(2019,5,2,9,30),    dateFin=datetime(2019,5,2,19,30),   prixInscription=3.5,    image="tankAttack.png",       nbrEquipeParMatch=2, tempsMatch=250, commandeLancement="url_serveur", repertoireCarte="url_cartes"))
    db.session.add(Concours(nomC="Angry bird",           dateDeb=datetime(2019,9,6,7,30),    dateFin=datetime(2019,9,6,20,30),   prixInscription=9.5,    image="angryBirds.png",       nbrEquipeParMatch=4, tempsMatch=100, commandeLancement="url_serveur", repertoireCarte="url_cartes"))
    db.session.add(Concours(nomC="Sauver peach",         dateDeb=datetime(2019,10,20,14,30), dateFin=datetime(2019,10,20,15,30), prixInscription=2.25,   image="sauverPeach.png",      nbrEquipeParMatch=4, tempsMatch=400, commandeLancement="url_serveur", repertoireCarte="url_cartes"))
    db.session.add(Concours(nomC="Le trône du roi",      dateDeb=datetime(2019,11,20,14,30), dateFin=datetime(2019,11,20,15,30), prixInscription=2.25,   image="le-trone-du-roi.jpg",  nbrEquipeParMatch=4, tempsMatch=400, commandeLancement="url_serveur", repertoireCarte="url_cartes"))
    db.session.add(Concours(nomC="Labyrinthe",           dateDeb=datetime(2020,2,29,8,30),   dateFin=datetime(2020,2,29,12,30),  prixInscription=2.25,   image="labyrinthe.png",       nbrEquipeParMatch=4, tempsMatch=400, commandeLancement="url_serveur", repertoireCarte="url_cartes"))
    db.session.add(Concours(nomC="Au resto",             dateDeb=datetime(2020,7,7,9,00),    dateFin=datetime(2020,7,7,17,30),   prixInscription=2.25,   image="restaurant.jpg",       nbrEquipeParMatch=4, tempsMatch=400, commandeLancement="url_serveur", repertoireCarte="url_cartes"))
    db.session.add(Concours(nomC="The Grinch",           dateDeb=datetime(2018,12,18,14,00), dateFin=datetime(2018,12,18,17,00), prixInscription=0,      image="le-grinch.jpg",        nbrEquipeParMatch=6, tempsMatch=100, commandeLancement="url_serveur", repertoireCarte="url_cartes"))
    db.session.add(Concours(nomC="Color battle",         dateDeb=datetime(2018,8,8,10,00),   dateFin=datetime(2018,8,8,12,00),   prixInscription=10,     image="paint.png",            nbrEquipeParMatch=4, tempsMatch=800, commandeLancement="url_serveur", repertoireCarte="url_cartes"))
    db.session.add(Concours(nomC="Strange he is",       dateDeb=datetime(2018,7,12,10,00),  dateFin=datetime(2018,7,12,14,00),  prixInscription=10,     image="docteur_strange.jpg",   nbrEquipeParMatch=4, tempsMatch=800, commandeLancement="url_serveur", repertoireCarte="url_cartes"))
    db.session.add(Concours(nomC="Hulk",                dateDeb=datetime(2018,3,19,10,00),  dateFin=datetime(2018,3,19,14,00),  prixInscription=10,     image="hulk.jpg",              nbrEquipeParMatch=4, tempsMatch=800, commandeLancement="url_serveur", repertoireCarte="url_cartes"))
    db.session.add(Concours(nomC="Gost World",          dateDeb=datetime(2018,1,30,10,00),  dateFin=datetime(2018,1,30,14,00),  prixInscription=10,     image="gost_world.png",        nbrEquipeParMatch=4, tempsMatch=800, commandeLancement="url_serveur", repertoireCarte="url_cartes"))
    db.session.add(Concours(nomC="Le Boss du Donjon",   dateDeb=datetime(2017,5,6,10,00),   dateFin=datetime(2017,5,6,14,00),   prixInscription=10,     image="le_boss_du_donjon.png", nbrEquipeParMatch=4, tempsMatch=800, commandeLancement="url_serveur", repertoireCarte="url_cartes"))

    # Equipe
    db.session.add(Equipe(idC=1, avatar="boss18.jpg",        nomE="Les boss du 18",         poste="info21-06"))
    db.session.add(Equipe(idC=1, avatar="goldenglove.png",   nomE="Les profs en guerre",    poste="info22-10"))
    db.session.add(Equipe(idC=1, avatar="pigigi.png",        nomE="Je sais pas",            poste="info23-09"))
    db.session.add(Equipe(idC=2, avatar="java.png",          nomE="Java star",              poste="info24-12"))
    db.session.add(Equipe(idC=2, avatar="creajeu.jpg",       nomE="CreaJeu",                poste="info25-16"))
    db.session.add(Equipe(idC=3, avatar="cookie.png",        nomE="Team Cookies",           poste="info26-01"))
    db.session.add(Equipe(idC=4, avatar="roi_roux.jpg",      nomE="IA",                     poste="info23-31"))
    db.session.add(Equipe(idC=4, avatar="codecademy.png",    nomE="codecademy",             poste="info07-04"))

    # Joueur
    db.session.add(Joueur(idE=1, idC=1, nomJ="Sion", prenomJ="Eva"))
    db.session.add(Joueur(idE=1, idC=1, nomJ="Thoma", prenomJ="Julien"))
    db.session.add(Joueur(idE=4, idC=2, nomJ="Sion", prenomJ="Emma"))
    db.session.add(Joueur(idE=4, idC=2, nomJ="Pourpre", prenomJ="Claude"))
    db.session.add(Joueur(idE=6, idC=3, nomJ="Lefèvre", prenomJ="Jean-Michel"))
    db.session.add(Joueur(idE=6, idC=3, nomJ="Morency", prenomJ="Arnault"))
    db.session.add(Joueur(idE=8, idC=4, nomJ="petit", prenomJ="Paule"))
    db.session.add(Joueur(idC=4, nomJ="Masse", prenomJ="Fionna"))
    db.session.add(Joueur(idC=4, nomJ="Lovaé", prenomJ="Chloé"))

    # Match
    db.session.add(Match(idC=1, phase="Terminé"))
    db.session.add(Match(idC=1, phase="Terminé"))
    db.session.add(Match(idC=1, phase="En cours"))
    db.session.add(Match(idC=2, phase="Terminé"))
    db.session.add(Match(idC=2, phase="En cours"))
    db.session.add(Match(idC=2, phase="Non commencé"))
    db.session.add(Match(idC=3, phase="Non commencé"))

    # Effectue
    db.session.add(Effectue(idM=1, idE=1, score=15))
    db.session.add(Effectue(idM=1, idE=2, score=16))
    db.session.add(Effectue(idM=2, idE=2, score=20))
    db.session.add(Effectue(idM=2, idE=3, score=21))
    db.session.add(Effectue(idM=3, idE=1, score=30))
    db.session.add(Effectue(idM=3, idE=3, score=5))
    db.session.add(Effectue(idM=4, idE=4, score=123))
    db.session.add(Effectue(idM=4, idE=5, score=68))
    db.session.add(Effectue(idM=5, idE=4, score=49))
    db.session.add(Effectue(idM=5, idE=5, score=97))
    db.session.add(Effectue(idM=6, idE=4, score=12))
    db.session.add(Effectue(idM=6, idE=5, score=230))
    db.session.add(Effectue(idM=7, idE=3, score=57))

    # Execution
    db.session.commit()

@app.cli.command()
def clean_db():
    """
    Supprime toutes les tables de la base de donnée.
    """
    db.drop_all()
    CliRunner().invoke(cli=crea_db)

@app.cli.command()
@click.argument('username')
@click.argument('password')
def newOrganisateur(username,password):
    """
    Ajoute un nouvel organisateur à la base de données
    """
    if len(password)<8 or len(password)>50:
        print("Le mot de passe doit contenir entre 8 et 50 caractères.")
    elif len(username)<3 or len(username)>50:
        print("Le pseudo doit contenir entre 3 et 50 caractères.")
    else:
        new_Organisateur(username, password)

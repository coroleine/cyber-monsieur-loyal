#### IMPORT ############
from .app import app
from .service_models import *
from .service_formulaire import *
from flask import render_template, request, redirect, url_for, json, abort
from flask_login import login_user, current_user, logout_user, login_required
from .pdf import create_pdf
from functools import wraps
import datetime
from flask_breadcrumbs import register_breadcrumb
########################




#### DECORATEUR ########
def admin_required(f):
	"""
		Autorise seulement les administrateurs
		à accéder à la page.
	"""
	@wraps(f)
	def decorated_function(*args, **kwargs):
		if not current_user.admin:
			return abort(403)
		return f(*args, **kwargs)
	return decorated_function


@login_manager.unauthorized_handler
def unauthorized_callback():
	"""
		Demande d'être authentifié pour
		accéder aux page du site.
	"""
	return redirect(url_for('connexion'))


def tournoi_finished_required(f):
	@wraps(f)
	def decorated_function(*args, **kwargs):
		concour = get_Concours(kwargs['idConcours'])
		if concour.dateFin < datetime.datetime.now():
			return abort(403)
		return f(*args, **kwargs)
	return decorated_function


def view_concours(*args, **kwargs):
	concours_id = request.view_args['idConcours']
	concours = get_Concours(concours_id)
	return [
		{'text': concours.nomC, 'url':url_for("ficheDetaillee", idConcours=concours.idC)}
	]

def view_lancer_concours(*args, **kwargs):
	concours_id = request.view_args['idConcours']
	concours = get_Concours(concours_id)
	match_num = request.view_args['numeroMatch']
	return [
		{'text': concours.nomC, 'url':url_for("ficheDetaillee", idConcours=concours.idC)},
		{'text': "Match " + str(match_num), 'url':url_for("lancerTournoi", idConcours=concours.idC, numeroMatch=match_num)}
	]

def view_resultat_match(*args, **kwargs):
	concours_id = request.view_args['idConcours']
	concours = get_Concours(concours_id)
	match_num = request.view_args['idMatch']
	return [
		{'text': concours.nomC, 'url':url_for("ficheDetaillee", idConcours=concours.idC)},
		{'text': "Résultat match " + str(match_num), 'url':url_for("resultatMatch", idConcours=concours.idC, idMatch=match_num)}
	]

def view_resultat_final(*args, **kwargs):
	concours_id = request.view_args['idConcours']
	concours = get_Concours(concours_id)
	return [
		{'text': concours.nomC, 'url':url_for("ficheDetaillee", idConcours=concours.idC)},
		{'text': "Résultat", 'url':url_for("resultatFinal", idConcours=concours.idC)}
	]

def view_modifier_concours(*args, **kwargs):
	concours_id = request.view_args['idConcours']
	concours = get_Concours(concours_id)
	return [
		{'text': concours.nomC, 'url':url_for("ficheDetaillee", idConcours=concours.idC)},
		{'text': "Modifier concours", 'url':url_for("gestionTournoi", idConcours=concours.idC)}
	]

def view_equipes(*args, **kwargs):
	concours_id = request.view_args['idConcours']
	concours = get_Concours(concours_id)
	return [
		{'text': concours.nomC, 'url':url_for("ficheDetaillee", idConcours=concours.idC)},
		{'text': "Equipes et joueurs", 'url':url_for("gestionEquipe", idConcours=concours.idC)}
	]

def view_publication(*args, **kwargs):
	concours_id = request.view_args['idConcours']
	concours = get_Concours(concours_id)
	return [
		{'text': concours.nomC, 'url':url_for("ficheDetaillee", idConcours=concours.idC)},
		{'text': "Publication", 'url':url_for("publication", idConcours=concours.idC)}
	]


########################




#### ERREURS ###########
@app.errorhandler(404)
@app.errorhandler(403)
@app.errorhandler(405)
def erreur(error):
	"""
		Prend en charge les erreurs de type:
		403, 404 et 405.
	"""
	return render_template(
		'erreur.html',
		pageName = error.name,
		application = get_Application_Active(),
		styleFiles = ["erreur.css"],
		organisateur = current_user,
		error = error,
		dirImgLogo = app.config["DIRECTORY_UTILES_IMAGE"]
		), error.code
########################




#### ROUTES ############

#### CONNEXION
@app.route("/connexion/", methods = ["GET","POST"])
def connexion():
	f = Login()
	if request.method == "POST":
		if f.validate_on_submit():
			user = f.get_authenticated_user()
		if user:
			login_user(user)
			return redirect(url_for("accueil"))
	return render_template(
		"connexion.html",
		styleFiles = ["connexion.css"],
		dirImgTheme = app.config["DIRECTORY_THEME_IMAGE"],
		form = f,
		application = get_Application_Active()
	)




#### DECONNEXION
@app.route("/logout/")
@login_required
def logout():
	logout_user()
	return redirect(url_for('connexion'))




#### ACCUEIL
@app.route("/")
@app.route("/accueil/")
@register_breadcrumb(app, '.', 'Accueil')
@login_required
def accueil():
	return render_template(
		"accueil.html",
		pageName="Accueil",
		application = get_Application_Active(),
		styleFiles=["accueil.css"],
		scriptFiles=["carousel.js"],
		listeConcours = get_prochain_concours(),
		listeArchive = get_archives_concours(),
		dirImgConcours = app.config["DIRECTORY_CONCOURS_IMAGE"],
		dirImgLogo = app.config["DIRECTORY_UTILES_IMAGE"],
		dirImgTheme = app.config["DIRECTORY_THEME_IMAGE"],
		organisateur = current_user,
		concoursActuel = get_concours_en_cours()
	)




#### NOUVEAU CONCOURS
@app.route("/nouveau-concours/")
@register_breadcrumb(app, '.nouveauConcours', 'Nouveau concours')
@login_required
def nouveauConcours():
	form_nouveau_concours = NouveauConcours()
	return render_template(
        "nouveauConcours.html",
        pageName = "Nouveau concours",
        application = get_Application_Active(),
		styleFiles = ["nouveauConcours.css"],
        scriptFiles = ["modals.js", "datePicker.js", "collapsible.js", "tooltips.js", "inputAvatar.js", "verificationFormulaireConcours.js"],
		dirImgTheme = app.config["DIRECTORY_THEME_IMAGE"],
		dirImgLogo = app.config["DIRECTORY_UTILES_IMAGE"],
		organisateur = current_user,
		form_nouveau_concours = form_nouveau_concours,
		concoursActuel = get_concours_en_cours()
    )

@app.route('/nouveau-concours/', methods = ['POST'])
@login_required
def saveNouveauConcours():
	form = NouveauConcours()
	if "avatar" in request.files:
		upload_image(request.files["avatar"], app.config["DIRECTORY_CONCOURS_IMAGE"])
	if "sujetConcours" in request.files and form.sujetConcours.data != None:
		nomPdf = app.config["DIRECTORY_CONCOURS_SUJET"] + newNomPdf(form.nom.data)
		upload_fichier(request.files["sujetConcours"], app.config["DIRECTORY_CONCOURS_SUJET"],newNomPdf(form.nom.data))
	if form.validate_on_submit:
		if "sujetConcours" not in request.files:
			nomPdf=None
		enregistrer_nouveau_concours(form,nomPdf)
	return redirect(url_for("accueil"))




#### ORGANISATEURS
@app.route("/organisateurs/")
@register_breadcrumb(app, '.organisateurs', 'Organisateurs')
@login_required
@admin_required
def organisateurs():
	f = GestionOrganisateur()
	return render_template(
		"organisateurs.html",
	    pageName = "Gestion des utilisateurs",
	    application = get_Application_Active(),
		styleFiles = ["organisateurs.css"],
		administrateurs = get_Admins(),
		organisateurs = get_Organisateurs(),
		form = f,
		dirImgTheme = app.config["DIRECTORY_THEME_IMAGE"],
		dirImgLogo = app.config["DIRECTORY_UTILES_IMAGE"],
		organisateur = current_user,
		concoursActuel = get_concours_en_cours()
	)

@app.route("/organisateurs/", methods = ["POST"])
@login_required
def organisateursSave():
	f = GestionOrganisateur()
	testAdmin = request.form.get('testAdmin')
	admin = 0
	if testAdmin:
		admin = 1
	new_Organisateur(f.username.data,f.password.data,1,admin)
	return redirect(url_for('organisateurs'))

@app.route("/organisateurs/<int:idOrganisateur>/")
@login_required
def organisateursDelete(idOrganisateur):
	get_Organisateur(idOrganisateur).delete()
	return redirect(url_for('organisateurs'))

@app.route("/organisateurs/modifier_<int:idOrganisateur>/",)
@login_required
def updateOrganisateur(idOrganisateur):
	update_organisateur(idOrganisateur)
	return redirect(url_for("organisateurs"))




#### PARAMETRES
@app.route("/parametres/")
@register_breadcrumb(app, '.parametres', 'Paramètres')
@login_required
def parametres():
	return render_template(
		"parametres.html",
		pageName="Paramètres",
		application = get_Application_Active(),
		styleFiles=["parametres.css"],
		scriptFiles = ["inputAvatar.js", "tooltips.js", "modals.js", "parametres.js"],
		form_application = ModifierApplication(),
		form_applicationNew = NouvelleApplication(),
		listeTheme = getAll_Theme(),
		listeAppli = getAll_Application(),
		dirImgTheme = app.config["DIRECTORY_THEME_IMAGE"],
		dirImgLogo = app.config["DIRECTORY_UTILES_IMAGE"],
		organisateur = current_user,
		concoursActuel = get_concours_en_cours()
	)

@app.route("/parametres/application/new/", methods = ["POST"])
@login_required
def newApplication():
	form = NouvelleApplication()
	if "logo" in request.files:
		upload_image(request.files["logo"], app.config["DIRECTORY_THEME_IMAGE"])
	if "fond" in request.files:
		upload_image(request.files["fond"], app.config["DIRECTORY_THEME_IMAGE"])
	if form.validate_on_submit:
		nouvelle_application(form)
	return redirect(url_for("parametres"))

@app.route("/parametres/application/modification/<int:idApplication>/", methods = ["POST"])
@login_required
def updateApplication(idApplication):
	form = ModifierApplication()
	modifier_application_actuelle(form, idApplication)
	return redirect(url_for("parametres"))

@app.route("/parametres/theme/<int:idTheme>/")
@login_required
def updateTheme(idTheme):
	changer_Theme(current_user, idTheme)
	return redirect(url_for("parametres"))

@app.route("/parametres/application/utilisateur/<int:idApplication>/")
@login_required
def choixApplication(idApplication):
	activer(idApplication)
	return redirect(url_for("parametres"))

@app.route("/parametres/application/supr/<int:idApplication>/")
@login_required
def suprApplication(idApplication):
	get_Application(idApplication).delete()
	return redirect(url_for("choixApplication", idApplication=1))


#### CONCOURS
@app.route("/concours/<int:idConcours>/")
@register_breadcrumb(app, '.ficheDetaillee', '', dynamic_list_constructor = view_concours)
@login_required
def ficheDetaillee(idConcours):
	concours = get_Concours(idConcours)
	return render_template(
		"ficheDetaillee.html",
		pageName="Fiche détaillée",
		application = get_Application_Active(),
		styleFiles = ["ficheDetaillee.css"],
		scriptFiles = ["modals.js"],
		dirImgEquipe = app.config["DIRECTORY_EQUIPE_IMAGE"],
		dirImgConcours = app.config["DIRECTORY_CONCOURS_IMAGE"],
		dirImgLogo = app.config["DIRECTORY_UTILES_IMAGE"],
		dirImgTheme = app.config["DIRECTORY_THEME_IMAGE"],
		dirSubject = app.config["DIRECTORY_CONCOURS_SUJET"],
		concours = concours,
		numeroMatch = concours.getNumeroMatch(),
		joueursSansEquipe = concours.get_Joueur_solo(),
		organisateur = current_user,
		concoursActuel = get_concours_en_cours(),
		dateJour = datetime.datetime.now(),
	)

@app.route("/concours/<int:idConcours>/", methods = ['POST'])
@login_required
def suprConcours(idConcours):
	get_Concours(idConcours).delete()
	return redirect(url_for("accueil"))




#### RESULTATS
@app.route("/concours/<int:idConcours>/resultats/")
@login_required
def exporterResultat(idConcours):
	concours = get_Concours(idConcours)
	equipes = getEquipeSortScore(concours)
	nombre_joueur = 0
	for equipe in equipes:
		nombre_joueur += len(equipe.joueurs)
	resultat = list()
	for i in range(len(equipes)):
		resultat.append({
			"place":i+1,
			"score":str(scoreTotal(concours, equipes[i])) + "pts",
			"avatar": equipes[i].avatar,
			"equipe": equipes[i].nomE,
			"listeJoueur": equipes[i].joueurs
		})
	return render_template(
		"resultat/resultatConcours.html",
		concours = concours,
		date = concours.getDateStr(),
		nombreEquipe = len(equipes),
		nombreJoueur = nombre_joueur,
		listeResultat = resultat,
		dirImgEquipe = app.config["DIRECTORY_EQUIPE_IMAGE"],
		dirImgConcours = app.config["DIRECTORY_CONCOURS_IMAGE"]
	)




#### LANCER MATCH
@app.route("/concours/<int:idConcours>/lancer-match/<int:numeroMatch>/", methods = ["GET", "POST"])
@register_breadcrumb(app, '.lancerTournoi', '', dynamic_list_constructor = view_lancer_concours)
@login_required
@tournoi_finished_required
def lancerTournoi(idConcours, numeroMatch=1):
	concours = get_Concours(idConcours)
	form = Jouer()
	if request.method == "POST":
		resultat_match = lancerConcours(concours, form)
		idM = concours.getNumeroMatch()-1
		enregistrer_resultats(resultat_match, idM, concours)
		return redirect(url_for('resultatMatch', idConcours=idConcours, idMatch=idM))
	else:
		equipes = choisir_equipes(concours)
		cartes = concours.cartes()
		form.setup_equipes(equipes)
		form.setup_cartes(cartes)
	return render_template(
		"lancerTournoi.html",
		pageName = "Lancer Match",
        application = get_Application_Active(),
        styleFiles = ["lancerTournoi.css"],
        scriptFiles = ["select.js", "listeCliquable.js"],
		dirImgEquipe = app.config["DIRECTORY_EQUIPE_IMAGE"],
		dirImgTheme = app.config["DIRECTORY_THEME_IMAGE"],
		dirImgLogo = app.config["DIRECTORY_UTILES_IMAGE"],
		listeEquipes = equipes,
		listeCartes = cartes,
		concours =concours,
		organisateur = current_user,
		numeroMatch = numeroMatch,
		form = form,
		concoursActuel = get_concours_en_cours()
	)




#### RESULTAT D'UN MATCH
@app.route("/concours/<int:idConcours>/resultat-match/<int:idMatch>/")
@register_breadcrumb(app, '.resultatMatch', '', dynamic_list_constructor = view_resultat_match)
@login_required
@tournoi_finished_required
def resultatMatch(idConcours, idMatch):
	return render_template(
        "resultatMatch.html",
		pageName = "Résultat Match",
		application = get_Application_Active(),
		dirImgEquipe = app.config["DIRECTORY_EQUIPE_IMAGE"],
		dirImgTheme = app.config["DIRECTORY_THEME_IMAGE"],
		dirImgLogo = app.config["DIRECTORY_UTILES_IMAGE"],
		styleFiles = ["resultatMatch.css"],
		listeNomEquipe = getEquipeMatchSortScore(idConcours, get_Concours(idConcours).matchs[idMatch-1].idM),
		concours = get_Concours(idConcours),
		organisateur = current_user,
		concoursActuel = get_concours_en_cours()
    )




#### RESULTAT DE FIN DE CONCOURS
@app.route("/concours/<int:idConcours>/resultat-final/")
@register_breadcrumb(app, '.resultatFinal', '', dynamic_list_constructor = view_resultat_final)
@login_required
@tournoi_finished_required
def resultatFinal(idConcours):
	c = get_Concours(idConcours)
	return render_template(
		"resultatFinal.html",
		pageName ="Résultat Final",
		application = get_Application_Active(),
		dirImgEquipe = app.config["DIRECTORY_EQUIPE_IMAGE"],
		dirImgTheme = app.config["DIRECTORY_THEME_IMAGE"],
		dirImgLogo = app.config["DIRECTORY_UTILES_IMAGE"],
		styleFiles = ["resultatFinal.css"],
		listeNomEquipe = getEquipeSortScore(c),
		concours = c,
		organisateur = current_user,
		concoursActuel = get_concours_en_cours()
    )




#### MODIFIER CONCOURS
@app.route("/concours/<int:idConcours>/modifier-concours/")
@register_breadcrumb(app, '.gestionTournoi', '', dynamic_list_constructor = view_modifier_concours)
@login_required
@tournoi_finished_required
def gestionTournoi(idConcours):
	formulaire = ModifierConcours()
	concours = get_Concours(idConcours)
	return render_template(
		"modifierConcours.html",
		pageName="Modifier concours",
		application = get_Application_Active(),
		styleFiles = ["nouveauConcours.css"],
		scriptFiles = ["datePicker.js", "collapsible.js", "tooltips.js", "inputAvatar.js", "verificationFormulaireConcours.js"],
		dirImgEquipe = app.config["DIRECTORY_EQUIPE_IMAGE"],
		dirImgConcours = app.config["DIRECTORY_CONCOURS_IMAGE"],
		dirImgLogo = app.config["DIRECTORY_UTILES_IMAGE"],
		dirImgTheme = app.config["DIRECTORY_THEME_IMAGE"],
		# dirImgLogo = app.config["DIRECTORY_UTILES_IMAGE"],
		organisateur = current_user,
		concoursActuel = get_concours_en_cours(),
		concours = concours,
		formulaire = formulaire
	)

@app.route("/concours/<int:idConcours>/modifier-concours/", methods = ["POST"])
@login_required
@tournoi_finished_required
def saveModifConcours(idConcours):
	form = ModifierConcours()
	if "avatar" in request.files and form.avatar.data != None :
		upload_image(request.files["avatar"], app.config["DIRECTORY_CONCOURS_IMAGE"])
	if "sujetConcours" in request.files and form.sujetConcours.data != None:
		nomPdf = app.config["DIRECTORY_CONCOURS_SUJET"] + newNomPdf(form.nom.data)
		upload_fichier(request.files["sujetConcours"], app.config["DIRECTORY_CONCOURS_SUJET"],newNomPdf(form.nom.data))
	if("sujetConcours" not in request.files ):
		nomPdf = None
	if form.validate_on_submit:
		enregistrer_modif_concours(form, nomPdf)
	return redirect(url_for("ficheDetaillee", idConcours=idConcours))




#### EQUIPES
@app.route("/concours/<int:idConcours>/equipes/")
@register_breadcrumb(app, '.gestionEquipe', '', dynamic_list_constructor = view_equipes)
@login_required
@tournoi_finished_required
def gestionEquipe(idConcours):
	concours = get_Concours(idConcours)
	form_modifier_equipe = ModifierEquipe()
	form_nouvelle_equipe = NouvelleEquipe()
	form_nouveau_joueur = NouveauJoueur()
	form_nouveau_joueur.setup_equipes(concours.equipes)
	return render_template(
        "gestionEquipes.html",
        pageName = "Gestion des équipes",
        application = get_Application_Active(),
        styleFiles = ["gestionEquipes.css"],
        scriptFiles = ["inputAvatar.js", "select.js", "modalEquipe.js", "avatar.js", "tooltips.js", "contexteMenu.js", "collapsible.js"],
		dirImgEquipe = app.config["DIRECTORY_EQUIPE_IMAGE"],
		dirImgTheme = app.config["DIRECTORY_THEME_IMAGE"],
		dirImgLogo = app.config["DIRECTORY_UTILES_IMAGE"],
        listeEquipes = concours.equipes,
		idConcours = idConcours,
		joueursSansEquipe = concours.get_Joueur_solo(),
		form_nouvelle_equipe = form_nouvelle_equipe,
		form_nouveau_joueur = form_nouveau_joueur,
		form_modifier_equipe = form_modifier_equipe,
		organisateur = current_user,
		concoursActuel = get_concours_en_cours()
    )

@app.route('/concours/<int:idConcours>/equipes/add-groups/', methods = ['POST'])
@login_required
@tournoi_finished_required
def saveNouvelleEquipe(idConcours):
	if "avatar" in request.files:
		upload_image(request.files["avatar"], app.config["DIRECTORY_EQUIPE_IMAGE"])
	form = NouvelleEquipe()
	if form.validate_on_submit:
		enregistrer_nouvelle_equipe(form, idConcours)
	return redirect(url_for("gestionEquipe", idConcours=idConcours))

@app.route('/concours/<int:idConcours>/equipes/update-groups/', methods = ['POST'])
@login_required
@tournoi_finished_required
def updateEquipe(idConcours):
	if "avatar" in request.files:
		upload_image(request.files["avatar"], app.config["DIRECTORY_EQUIPE_IMAGE"])
	form = ModifierEquipe()
	if form.validate_on_submit:
		modifier_equipe(form, idConcours)
	return redirect(url_for("gestionEquipe", idConcours=idConcours))

@app.route('/concours/<int:idConcours>/equipes/add-player/', methods = ['POST'])
@login_required
@tournoi_finished_required
def saveNouveauJoueur(idConcours):
	form = NouveauJoueur()
	if form.validate_on_submit:
		enregistrer_nouveau_joueur(form, idConcours)
	return redirect(url_for("gestionEquipe", idConcours=idConcours))

@app.route('/concours/<int:idConcours>/equipes/load-group/', methods = ['POST'])
@login_required
@tournoi_finished_required
def loadModal(idConcours):
	idEquipe = request.form["idEquipe"]
	equipe = get_Equipe(idEquipe)
	liste_joueur = [joueur.dico() for joueur in get_Equipe(idEquipe).joueurs]
	return json.dumps({
		"avatar": equipe.avatar,
		"nom": equipe.nomE,
		"poste": equipe.poste,
		"joueurs": liste_joueur
	})

@app.route('/concours/<int:idConcours>/equipes/delete-player/', methods = ['POST'])
@login_required
@tournoi_finished_required
def removeJoueur(idConcours):
	return json.dumps({"delete": get_Joueur(request.form["idJoueur"]).delete()})

@app.route('/concours/<int:idConcours>/equipes/delete-group/', methods = ['POST'])
@login_required
@tournoi_finished_required
def removeEquipe(idConcours):
	return json.dumps({"delete": get_Equipe(request.form["idEquipe"]).delete()})

@app.route('/concours/<int:idConcours>/equipes/<int:idEquipe>/joueur/<int:idJoueur>/')
@login_required
@tournoi_finished_required
def changerEquipe(idConcours, idJoueur, idEquipe):
	get_Joueur(idJoueur).setEquipe(idEquipe)
	return redirect(url_for("gestionEquipe", idConcours=idConcours))




#### ARRETER UN CONCOURS
@app.route("/concours/<int:idConcours>/terminee/")
@login_required
def arreterTournoi(idConcours):
	get_Concours(idConcours).finished()
	return redirect(url_for('resultatFinal', idConcours=idConcours))




#### PUBLICATION
@app.route("/concours/<int:idConcours>/publication/")
@register_breadcrumb(app, '.publication', '', dynamic_list_constructor = view_publication)
@login_required
def publication(idConcours):
	return render_template(
		"publication.html",
		pageName = "Publication",
        application = get_Application_Active(),
		concours = get_Concours(idConcours),
        styleFiles = ["publication.css"],
        scriptFiles = ["select.js", "inputAvatar.js", "tooltips.js", "publierFacebook.js", "sdkFacebook.js"],
		dirImgTheme = app.config["DIRECTORY_THEME_IMAGE"],
		dirImgLogo = app.config["DIRECTORY_UTILES_IMAGE"],
		organisateur = current_user,
		concoursActuel = get_concours_en_cours()
	)

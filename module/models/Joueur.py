#### HEAD ##############
# Class   : Joueur
# Content : Table JOUEUR


#### IMPORT ############
from ..app import db


#### CLASS #############
class Joueur(db.Model):
	idJ 		= db.Column(db.Integer, primary_key=True, autoincrement=True)
	idC 		= db.Column(db.Integer, db.ForeignKey("concours.idC"), nullable=False)
	idE 		= db.Column(db.Integer, db.ForeignKey("equipe.idE"))
	nomJ 		= db.Column(db.String(20))
	prenomJ 	= db.Column(db.String(20))

	def __repr__(self):
		return "<Joueur (%d) %s>" % (self.idJ, self.nomJ)

	def setEquipe(self, id):
		"""
			Définit une équipe au joueur
		"""
		self.idE = id
		db.session.commit()

	def dico(self):
		"""
			Retourne le joueur sous forme d'un dictionnaire
			:return: le joueur sous forme d'un dictionnaire
			:rtyoe: dict

		"""
		return {"idJ": self.idJ, "nomJ": self.nomJ, "prenomJ":self.prenomJ}

	def delete(self):
		"""
			Permet la supression de l'objet
		"""
		try:
			db.session.delete(self)
			db.session.commit()
			return True
		except:
			return False

#### GETTEURS ##########
def getAll_Joueur():
	"""
		Renvoie toute la table

		:return: La liste de toutes les valeurs de la table
		:rtype: list
	"""
	return Joueur.query.all()

def get_Joueur(idJ):
	"""
		Renvoie l'équipe spécifique à l'identifiant

		:param idJ: L'identifiant du joueur
		:type idJ: int
		:return: Un joueur
		:rtype: Object
	"""
	return Joueur.query.get(idJ)

def getIdMax_joueur():
    return Joueur.query.all()[-1].idJ + 1

#### ADD ############
def new_Joueur(nomJ, prenomJ, idC, idE):
	"""
		Ajoute l'occurence pour lequel la valeur correspond
		au paramètre.

		:param idJ: L'identifiant du joueur
		:type idJ: int
		:param nomJ: Le nom du joueur
		:type nomJ: str
		:param prenomJ: Le prénom du joueur
		:type prenomJ: str
		:return: Si l'oppération c'est bien déroulé
		:rtyoe: boolean
	"""
	try:
		if idE == "None":
			c = Joueur(nomJ=nomJ, prenomJ=prenomJ, idC=idC)
		else:
			c = Joueur(nomJ=nomJ, prenomJ=prenomJ, idC=idC, idE=idE)
		db.session.add(c)
		db.session.commit()
		return True
	except:
		return False

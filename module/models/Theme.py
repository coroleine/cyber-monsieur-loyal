#### HEAD ##############
# Class   : Theme
# Content : Table THEME


#### IMPORT ############
from ..app import db

class Theme(db.Model):
    idT         = db.Column(db.Integer, primary_key=True, autoincrement=True)
    couleurT    = db.Column(db.String(7))

    def __repr__(self):
        return "<Theme (%d) %s>" % (self.idT, self.couleurT)

    def delete(self):
        """
            Permet la supression de l'objet
        """
        try:
            db.session.delete(self)
            db.session.commit()
            return True
        except:
            return False


#### GETTEURS ##########
def getAll_Theme():
    """
        Renvoie toute la table

        :return: La liste de toutes les valeurs de la table
        :rtype: list
    """
    return Theme.query.all()

def get_Theme(idT):
    """
        Renvoie le thème spécifique à l'identifiant

        :param idT: L'identifiant du thème
        :type idT: int
        :return: Un thème
        :rtype: Object
    """
    return Theme.query.get(idT)


#### ADD ############
def new_Theme(couleurT):
        """
            Ajoute l'occurence pour lequel la valeur correspond
            au paramètre.

            :param couleurT: La couleur de theme
            :type couleurT: string
            :return: Si l'oppération c'est bien déroulé
            :rtyoe: boolean
        """
        try:
            db.session.add(Theme(couleurT=couleurT))
            db.session.commit()
            return True
        except:
            return False

#### HEAD ##############
# Class   : Application
# Content : Table APPLICATION


#### IMPORT ############
from ..app import db

class Application(db.Model):
    idA                     = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nomA                    = db.Column(db.String(40), unique=True)
    backgroundConnexionA    = db.Column(db.String(250))
    logoA                   = db.Column(db.String(250))
    active                  = db.Column(db.Integer)

    def __repr__(self):
        return "<Application (%d) %s>" % (self.idA, self.nomA)

    def delete(self):
        """
            Permet la supression de l'objet
        """
        try:
            db.session.delete(self)
            db.session.commit()
            return True
        except:
            return False


#### GETTEURS ##########
def getAll_Application():
    """
        Renvoie toute la table

        :return: La liste de toutes les valeurs de la table
        :rtype: list
    """
    return Application.query.all()

def get_Application(idA):
    """
        Renvoie le application spécifique à l'identifiant

        :param idT: L'identifiant du application
        :type idT: int
        :return: Une application
        :rtype: Object
    """
    return Application.query.get(idA)

def get_Application_Active():
    """
        Retourne l'application actuelle active

        :return: Une application
        :rtype: Object
    """
    return Application.query.filter_by(active=1).first()


#### ADD ############
def new_Application(nomA="L'arène", backgroundConnexionA="terre.jpg", logoA="logo.jpg"):
        """
            Ajoute l'occurence pour lequel la valeur correspond
            au paramètre.

            :param nomT: Le nom pour l'application
            :type nomT: str
            :param backgroundConnexionT: L'image de fond pour page connexion
            :type backgroundConnexionT: str
            :param logoT: Le logo de l'application
            :type logoT: str
            :return: Si l'oppération c'est bien déroulé
            :rtyoe: boolean
        """
        try:
            application = Application(nomA=nomA, backgroundConnexionA=backgroundConnexionA, logoA=logoA, active=0)
            db.session.add(application)
            db.session.commit()
            return True
        except:
            return False

#### HEAD ##############
# Class   : Equipe
# Content : Table EQUIPE


#### IMPORT ############
from ..app import db


#### CLASS #############
class Equipe(db.Model):
    idE         = db.Column(db.Integer, primary_key=True, autoincrement=True)
    idC         = db.Column(db.Integer, db.ForeignKey("concours.idC"), nullable=False)
    avatar      = db.Column(db.String(250))
    nomE        = db.Column(db.String(50))
    poste       = db.Column(db.String(15))
    joueurs     = db.relationship("Joueur", backref="equipe", cascade="all, delete-orphan")
    effectues   = db.relationship("Effectue", backref="equipe", cascade="all, delete-orphan")

    @property
    def matchs(self):
        """
            Retourne la liste des matchs de l'équipe
            :return: la liste des matchs de l'équipe
            :rtyoe: liste d'objets
        """
        res=[]
        for e in self.effectues:
            res.append(e.match)
        return res

    def __repr__(self):
        return "<Equipe (%d) %s>" % (self.idE, self.nomE)

    def delete(self):
        """
            Permet la supression de l'objet
        """
        try:
            db.session.delete(self)
            db.session.commit()
            return True
        except:
            return False

    def score(self):
        """
            Retourne le score actuel de l'équipe
            :return: le score actuel de l'équipe
            :rtyoe: int
        """
        res = 0
        for e in self.effectues:
            res += e.score
        return res




#### GETTEURS ##########
def getAll_Equipe():
    """
        Renvoie toute la table

        :return: La liste de toutes les valeurs de la table
        :rtype: list
    """
    return Equipe.query.all()


def get_Equipe(idE):
    """
        Renvoie l'équipe spécifique à l'identifiant

        :param idE: L'identifiant de l'équipe
        :type idE: int
        :return: Une équipe
        :rtype: Object
    """
    return Equipe.query.get(idE)

def new_Equipe(idC, nomE, avatar, poste):
        """
            Ajoute l'occurence pour lequel la valeur correspond
            au paramètre.

            :param idC: L'identifiant du concours
            :type idC: int
            :param nomE: Le nom pour l'équipe
            :type nomE: str
            :param avatar: L'image de fond pour page connexion
            :type avatar: str
            :param poste: Le logo de l'application
            :type poste: str
            :return: Si l'oppération c'est bien déroulé
            :rtyoe: boolean
        """
        try:
            application = Equipe(idC=idC, nomE=nomE, avatar=avatar, poste=poste)
            db.session.add(application)
            db.session.commit()
            return True
        except:
            return False

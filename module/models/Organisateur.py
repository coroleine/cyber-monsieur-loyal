#### HEAD ##############
# Class   : Organisateur
# Content : Table ORGANISATEUR


#### IMPORT ############
from ..app import db
from flask_login import UserMixin
from hashlib import sha256

#### CLASS #############
class Organisateur(db.Model, UserMixin):
    id              = db.Column(db.Integer, primary_key=True, autoincrement=True)
    loginO          = db.Column(db.String(50), unique=True)
    mdpO            = db.Column(db.String(100))
    admin           = db.Column(db.Integer(), default=0)
    idTheme         = db.Column(db.Integer, db.ForeignKey("theme.idT"))
    theme           = db.relationship("Theme", backref="organisateur")

    def __repr__(self):
        return "<Organisateur (%d) %s>" % (self.id, self.loginO)

    def delete(self):
        """
            Permet la supression de l'objet
        """
        try:
            db.session.delete(self)
            db.session.commit()
            return True
        except:
            return False



#### GETTEURS ##########
def getAll_Organisateur():
    """
        Renvoie toute la table

        :return: La liste de toutes les valeurs de la table
        :rtype: list
    """
    return Organisateur.query.all()

def get_Admins():
    """
        Renvoie les organisateurs qui sont des administrateurs

        :return: La liste de touts les administrateurs de la table
        :rtype: list
    """
    return Organisateur.query.filter(Organisateur.admin == 1).all()

def get_Organisateurs():
    """
        Renvoie les organisateurs qui ne sont pas des administrateurs

        :return: La liste de touts les organisateurs de la table
        :rtype: list
    """
    return Organisateur.query.filter(Organisateur.admin == 0).all()

def get_Organisateur(idO):
    """
        Renvoie l'organisateur spécifique à l'identifiant

        :param idO: L'identifiant de l'organisateur
        :type idO: int
        :return: Un organisateur
        :rtype: Object
    """
    return Organisateur.query.get(idO)


#### ADD ############
def new_Organisateur(loginO, mdpO, idTheme=1, admin=0):
    """
        Ajoute l'occurence pour lequel la valeur correspond
        au paramètre.

        :param idO: L'identifiant de l'organisateur
        :type idO: int
        :param loginO: Le login de l'organisateur
        :type loginO: str
        :param mdpO: Le hash du mot de passe de l'organisateur
        :type mdpO: str
        :return: Si l'oppération c'est bien déroulé
        :rtyoe: boolean
    """
    try:
        mdp = sha256(mdpO.encode()).hexdigest()
        o = Organisateur(loginO=loginO, mdpO=mdp, admin=admin, idTheme=idTheme)
        db.session.add(o)
        db.session.commit()
        return True
    except:
        return False

def update_organisateur(idO):
    """
        Change l'organisateur en administrateur et vice versa

        :param idO: L'identifiant de l'organisateur
        :type idO: int
    """
    orga = get_Organisateur(idO)
    if orga.admin == 0:
        orga.admin = 1
    else:
        orga.admin = 0
    db.session.commit()

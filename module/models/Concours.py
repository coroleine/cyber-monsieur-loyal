#### HEAD ##############
# Class   : Concours
# Content : Table CONCOURS


#### IMPORT ############
from ..app import db
from os import listdir
import traceback

#### CLASS #############
class Concours(db.Model):
    idC                 = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nomC                = db.Column(db.String(40))
    image               = db.Column(db.String(250))
    dateDeb             = db.Column(db.DateTime)
    dateFin             = db.Column(db.DateTime)
    prixInscription     = db.Column(db.Float)
    sujetC              = db.Column(db.String(250))
    resultats           = db.Column(db.String(100))
    nbrEquipeParMatch   = db.Column(db.Integer)
    tempsMatch          = db.Column(db.Integer)
    commandeLancement   = db.Column(db.String(100))
    port                = db.Column(db.Integer())
    repertoireCarte     = db.Column(db.String(100))
    termine             = db.Column(db.Integer)
    equipes             = db.relationship("Equipe", backref="concours", cascade="all, delete-orphan")
    joueurs             = db.relationship("Joueur", backref="concours", cascade="all, delete-orphan")
    matchs              = db.relationship("Match", backref="concours", cascade="all, delete-orphan")

    def __repr__(self):
        return "<Concours (%d, %s)>" % (self.idC, self.nomC)

    def delete(self):
        """
            Permet la supression de l'objet
        """
        try:
            db.session.delete(self)
            db.session.commit()
            return True
        except:
            return False

    def getDateStr(self):
        """
            retourne la Date en format 'jj mois aaaa'

            :return: retourn les date formalisé
            :rtype: String
        """
        mois = {1:"Janvier", 2:"Février", 3:"Mars", 4:"Avril", 5:"Mai",
                6:"Juin", 7:"Juillet", 8:"Août", 9:"Septembre",
                10:"Octobre", 11:"Novembre", 12:"Décembre"}
        return "{} {} {}".format(self.dateDeb.day, mois[self.dateDeb.month], self.dateDeb.year)

    def get_Joueur_solo(self):
        """
            retourne les joueurs sans equipe

            :return: la liste des joueurs sans équipe
            :rtype: List
        """
        joueurs_solo = list()
        for joueur in self.joueurs:
            if joueur.idE == None:
                joueurs_solo.append(joueur)
        return joueurs_solo

    def cartes(self):
        """
            retourne la liste des cartes

            :return: la liste des cartes
            :rtype: Dict
        """
        cartes = dict()
        try:
            liste_fichier = listdir(path=self.repertoireCarte)
        except:
            return None
        for fichier in liste_fichier:
            cartes[fichier.split(".")[0]] = fichier
        return cartes

    def getNumeroMatch(self):
        """
            retourne l'id du match

            :return: l'id du match
            :rtype: int
        """
        return len(self.matchs)+1

    def isFinished(self):
        """
            retourne si le match est fini

            :return: match fini
            :rtype: Boolean
        """
        matchs = self.matchs
        if(len(matchs) == 0):
            return 1
        indice = 0
        while(indice < len(matchs) and matchs[indice].phase != "En cours"):
            indice += 1
        return indice + 1

    def finished(self):
        """
            insert dans la BD si le match est terminé
        """
        self.termine = 1
        db.session.commit()



#### GETTEURS ##########
def getAll_Concours():
    """
        Renvoie toute la table

        :return: La liste de toutes les valeurs de la table
        :rtype: list
    """
    return Concours.query.all()


def get_Concours(idC):
    """
        Renvoie le concours spécifique à l'identifiant

        :param idC: L'identifiant du concours
        :type idC: int
        :return: Une équipe
        :rtype: Object
    """
    return Concours.query.get(idC)




#### ADD ###############
def new_Concours(nom, image, dateDebut, dateFin, prix, sujet, nbrEquipeParMatch, temps, commandeLancement, port, repertoireCarte):
    """
        Ajoute l'occurence pour lequel la valeur correspond
        au paramètre.

        :param nom: Le nom pour l'application
        :type nom: str
        :param image: L'image de fond pour page connexion
        :type image: str
        :param dateDebut: La date de début du concours
        :type dateDebut: datetime
        :param dateFin: La date de fin du concours
        :type dateFin: datetime
        :param prix: Le prix d'inscription au concours
        :type prix: float
        :param sujet: Le nom du fichier du sujet
        :type sujet: str
        :param nbrEquipeParMatch: Le nombre de participant par match
        :type nbrEquipeParMatch: int
        :param temps: La durée d'un match en seconde
        :type temps: int
        :param commandeLancement: La commande lançant un match
        :type commandeLancement: str
        :param port: Le port du serveur de jeu
        :type port: int
        :param repertoireCarte: Le lien vers le dossier contenant les cartes de jeu
        :type repertoireCarte: str
        :return: Si l'oppération c'est bien déroulé
        :rtyoe: boolean
    """
    try:
        concours = Concours(
            nomC                = nom,
            image               = image,
            dateDeb             = dateDebut,
            dateFin             = dateFin,
            prixInscription     = prix,
            sujetC              = sujet,
            nbrEquipeParMatch   = int(nbrEquipeParMatch),
            tempsMatch          = temps,
            commandeLancement   = commandeLancement,
            port                = port,
            repertoireCarte     = repertoireCarte,
            termine             = 0
        )
        db.session.add(concours)
        db.session.commit()
        return True
    except:
        traceback.print_exc()
        return False
